<?php
/**
 * The template for displaying the footer.
 *
 * @package Great
 */

// Theme Footer Copyright Info-Text
?>

<div id="contact-footer">
	<div class="row">
		<div class="col-sm-4">
			<h2>
				<img src="/wordpress/wp-content/uploads/2017/03/icon-des.jpg">
			</h2>
			<div class="row">
				<?php
				$args = array('posts_per_page' => 6,'category_name' =>'ha-noi','orderby'=> 'id','order'=>'desc');

				$myposts = new WP_Query($args);
				if ( $myposts -> have_posts() ) :
				    while ( $myposts -> have_posts() ) :$myposts -> the_post(); ?>
				    	<div class="col-sm-4">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
				    <?php
				    endwhile;
				endif
				?>
			</div>
		</div>
		<div class="col-sm-4">
			<h2>
				<img src="/wordpress/wp-content/uploads/2017/03/travel_home.png">
			</h2>
			<?php 
				$args = array(
					'posts_per_page' => '6',
					'orderby' 		 => 'date',
					'order'			 => 'desc'
					);
				$recent_posts = new WP_Query($args);
				
				if ( $recent_posts -> have_posts() ) :
				    while ( $recent_posts -> have_posts() ) :$recent_posts -> the_post(); ?>
				    	<div class="title-footer">
				    		<a href="<?php the_permalink(); ?>"><span class="fa fa-check"></span> <?php the_title(); ?></a>
				    	</div>
				    <?php
				    endwhile;
				endif;
			?>
		</div>
		<div class="col-sm-4">
			<h2>
				<img src="/wordpress/wp-content/uploads/2017/03/quick.png">
			</h2>
			<?php echo do_shortcode('[contact-form-7 id="1297" title="Contact Us"]'); ?>
		</div>
	</div>
</div>
<div class="menu-footer">
	<?php if ( get_theme_mod('menu_hide') != 1 ):?>
		<?php great_footer_menu ();?>
        <!-- Responsive Menu -->
        <div class="responsive-menu-bar open-responsive-menu"><i class="fa fa-bars"></i> <span><?php echo __('Menu', 'great');?></span></div>
        <div id="responsive-menu">
            <div class="menu-close-bar open-responsive-menu"><i class="fa fa-times"></i> <?php echo __('Close', 'great');?></div>
        </div>
        <div class="clear"></div>
    <?php endif;?>
</div>
<div class="entry-footer">
	<h3>Wati travel - Best choice for Hanoi day tours & surroundings</h3>
	<p>
		Head office: 34 Hang Non Str, Hoan Kiem, Hanoi, Vietnam <br />
		Tel:     Fax:   +84439955678 <br />
		Email: info@watitravel.com
	</p>
	<span>International Bussiness License: 01-713/2015 TCDL - GPLHQT</span>
</div>
<?php
wp_footer(); ?>
</body>
</html>
