jQuery(document).ready(function($) {
	$('ul.products').addClass('row');
	$('#one-day-tour ul.products li').addClass('col-sm-6 col-xs-12');
	$('.recent-product ul.products li').addClass('col-xs-12');
	$('div#one-day-tour ul li div').removeClass('row');
	$('div#one-day-tour ul li div').removeClass('col-sm-3');
	$('div#one-day-tour ul li div').removeClass('col-sm-6');

	$('div.related ul li div').removeClass('row');
	$('div.related ul li div').removeClass('col-sm-3');
	$('div.related ul li div').removeClass('col-sm-6');

	$('div.wpcf7 input').removeAttr('size');
	$('div.wpcf7 textarea').attr('rows','3');

});

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1268055039943553";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
