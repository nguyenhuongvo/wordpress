<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Great

 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
        <div id="logo">
            <div class="row">       
                <div class="col-sm-4" id="logo-img">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    <img
                        src="<?php echo esc_url( get_theme_mod('logo' , get_template_directory_uri() . '/images/logo.png') ); ?>"
                        alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                        />
                    </a>
                </div><!-- #header-logo-image -->
                <div class="col-sm-6 col-sm-offset-2" id="contact-header">
                    <div class="phone-header textleft">
                        <span class="fa fa-phone"></span>
                        <span style="color: yellow;"> (84) 943 887 393</span>
                        <span> | </span>
                        <span class="fa fa-skype" style="color: blue;;"></span>
                    </div>
                    <div class="location-header textleft">
                        <span class="fa fa-map-marker"></span>
                        <span> 17 Hang Hanh Str, Hoan Kiem, Hanoi Old Quater, Vietnam</span>
                    </div>
                    <div class="gmail-header textleft">
                        <span class="fa fa-envelope-o"></span>
                        <span> info@madammoonguesthouse.com</span>
                    </div>
                </div>
            </div> 
            
            <div class="clear"></div>
        </div><!-- .site-branding -->
    </header><!-- #masthead -->
    
    <?php if ( get_theme_mod('menu_hide') != 1 ):?>
		<?php great_header_menu ();?>
        <!-- Responsive Menu -->
        <div class="responsive-menu-bar open-responsive-menu"><i class="fa fa-bars"></i> <span><?php echo __('Menu', 'great');?></span></div>
        <div id="responsive-menu">
            <div class="menu-close-bar open-responsive-menu"><i class="fa fa-times"></i> <?php echo __('Close', 'great');?></div>
        </div>
        <div class="clear"></div>
    <?php endif;?>

    <div class="slider-header">
    <?php echo do_shortcode('[easingslider id="1322"]'); ?>
        
    </div>

<div id="content" class="site-content">
