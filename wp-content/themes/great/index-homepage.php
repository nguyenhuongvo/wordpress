<?php
/**
 * Homepage index file.
 *
 * @package Great

 */

get_header(); ?>
		<?php if(is_home()): ?>
			<div class="content-featured" id="one-day-tour">
				<?php echo do_shortcode('[featured_products per_page="3" columns="3" orderby="id" order="asc"]') ?>
			</div>
		<?php endif; ?>
	<div id="primary" class="col-sm-9">
		<main id="main" class="site-main" role="main">
		
		<div class="recent-product">
			<img src="/wordpress/wp-content/uploads/2017/03/texttourbest1.png">
			<?php echo do_shortcode('[products per_page="3" columns="1" orderby="date" order="desc"]') ?>
		</div>
		<div class="featured_post row">
		<?php 
			$args = array(
				'showposts' => '1'
				);
			$recent_posts = new WP_Query($args);
			
			if ( $recent_posts -> have_posts() ) :
			    while ( $recent_posts -> have_posts() ) :$recent_posts -> the_post(); ?>
			    	<p class="title-featured-post"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></p>
			    	<div class="border-img">
			    		<img src="/wordpress/wp-content/uploads/2017/03/gachngang.png">
			    	</div>
			    	<div class="row">
			    		<div class="col-sm-6">
			    			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
			    		</div>
			    		<div class="col-sm-6" style="color: #7D4206">
			    			<?php the_excerpt(); ?>
			    			<a class="btn btn-primary" href="<?php the_permalink(); ?>" style="background: green !important;border:none;">Learn More</a>
			    		</div>
			    	</div>
			    <?php
			    break;
			    endwhile;
			endif;
		?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php //get_footer(); ?>
